﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject projectilePrefab;
    [SerializeField] private GameObject[] targetsPrefabs;

    [SerializeField] private Transform projectileSpawnPoint;
    [SerializeField] private Transform targetsSpawnPoint;

    private Vector2 startSwipePosition;
    public bool isInFlight { get; set; }
    public Transform projectileInstance { get; set; }

    private System.Random rnd;

    void Start()
    {
        this.rnd = new System.Random();
        this.ResetLevel();
    }

    void Update()
    {
        if (!isInFlight)
        {
            if (Input.GetMouseButtonDown(0))
            {
                this.startSwipePosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            }

            if (Input.GetMouseButtonUp(0))
            {
                this.Swipe(Camera.main.ScreenToViewportPoint(Input.mousePosition));
            }
        }
    }

    public void ResetLevel()
    {
        (from Transform child in this.projectileSpawnPoint select child.gameObject).ToList().ForEach(GameObject.Destroy);
        (from Transform child in this.targetsSpawnPoint select child.gameObject).ToList().ForEach(GameObject.Destroy);

        this.SpawnProjectile();
        this.SpawnTargets();
    }

    public void SpawnProjectile()
    {
        this.projectileInstance = GameObject.Instantiate(this.projectilePrefab).transform;
        this.projectileInstance.SetParent(this.projectileSpawnPoint);
        this.projectileInstance.localPosition = Vector3.zero;
    }

    public void SpawnTargets()
    {
        var instance = GameObject.Instantiate(this.targetsPrefabs[this.rnd.Next(0, this.targetsPrefabs.Length)]).transform;
        instance.SetParent(this.targetsSpawnPoint);
    }

    private void Swipe(Vector2 endSwipePosition)
    {
        var swipeVector = endSwipePosition - this.startSwipePosition;
        if (swipeVector.magnitude < 0.01) return;
        this.projectileInstance.GetComponent<Rigidbody>().AddForce(new Vector3(swipeVector.x * 4, swipeVector.y, 1) * 10, ForceMode.Impulse);
        this.isInFlight = true;
        this.StartCoroutine(this.Flight());
    }

    private IEnumerator Flight()
    {
        yield return new WaitForSeconds(2);

        GameObject.DestroyImmediate(projectileInstance.gameObject);
        this.SpawnProjectile();
        this.isInFlight = false;
    }
}
